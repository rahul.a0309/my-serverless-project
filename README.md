# Serverless Architecture using Spring Cloud Function + AWS Lambda & AWS API Gateway!
Serverless architectures are application designs that incorporate third-party “Backend as a Service” (BaaS) services, and/or that include custom code run in managed, ephemeral containers on a “Functions as a Service” (FaaS) platform. By using these ideas, and related ones like single-page applications, such architectures remove much of the need for a traditional always-on server component. Serverless architectures may benefit from significantly reduced operational cost, complexity, and engineering lead time, at a cost of increased reliance on vendor dependencies and comparatively immature supporting services.

Reference - https://martinfowler.com/articles/serverless.html

## Project - This is a small example of serverless architecture using Spring Cloud function which is deployed on AWS Lambda and those function can be invoked through API Gateway.
### Use Cases -
###### Function 1: uppercaseFunction - Return given string to upperCase.
###### Function 2: greeter - Return given string with a greeting message.
  
### Main Function
```sh
@SpringBootApplication
public class MainFunctionApplication {

   public static void main(String[] args) throws Exception {
        SpringApplication.run(MainFunctionApplication.class, args);
    }
}
```

### Function which implement to mainFunction
```sh
@Component("uppercaseFunction")
public class UppercaseFunction implements Function<ApplicationRequest, ApplicationResponse> {...}

@Component("greeter")
public class GreeterFunction implements Function<ApplicationRequest, ApplicationResponse> {...}
```
# AWS Lambda Handler
A Handler class which just implements SpringBootRequestHandler is needed.<br/>
**AWS Lambda:** This is pay-as-you-go serverless compute service. So you don't have to manage the server just deploy your soruce code to AWS Lambda and they handle the rest.  But, want to know what's even better? It scales automatically and has absolutely no downtime.<br/>
The **technical definition** for AWS Lambda would be a Function as a Service. You deploy some code, it gets invoked, processes some input, and returns a value.

# Getting Started
### Generate deployable Artifact(.jar) -  
Run following command to generate deployable/uploadable .jar file.
```sh
mvn clean package install
```

After successfully execution of the above command you can see the target folder with .jar files.

# Run Locally
Run below command to test the Spring Cloud Function locally -
```sh
mvn spring-boot:run
```
This command run the project on localhost:8080 (add custom port in the application.property file).

### Invole the functions -

###### Function 1 - uppercaseFunction
```sh
curl localhost:8080/uppercaseFunction -H "Content-Type: application/json" -d '{"input":"hello"}'
{"result":"HELLO"}
```
###### Function 2 - greeter
```sh
curl localhost:8080/greeter -H "Content-Type: application/json" -d '{"input":"Rahul"}'
{"result":"Hello Rahul, and welcome to Spring Cloud Function!!!"}
```

# Deploy on AWS Lambda and configure API Gateway!

###### Create two Lambda functions (since we've two functions) through AWS Console (Note: Later will create through .yml file).

![alt Lambda Function](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/061103d45dbb59ef84ca984e9186976c/Screen_Shot_2018-11-14_at_7.54.59_PM.png)

###### Configure these Lambda functions with following steps -
  - Upload the same jar file to both functions (jar with aws).
  - Provide fully qualified handler name - eg. com.serverless.application.handler.aws.ApplicationFunctionHandler
  - Add the FUNCTION_NAME accordingly into these Lambda functions.
  - Add test casse and test your Lambda function.

  
###### Refer below screenshots -
![alt Lambda](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/f1a75b30352242a083a17aa270c522dd/Screen_Shot_2018-11-14_at_8.05.35_PM.png)
```sh 

```
![alt Lambda](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/e6dbcb0de4c46ea0172e9ff933a5d697/Screen_Shot_2018-11-14_at_8.07.23_PM.png)
```sh 

```
![alt Lambda](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/c0c7321562d21612e7261f757c17666e/Screen_Shot_2018-11-14_at_8.15.12_PM.png)
```sh 

```
![alt Lambda](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/d5bae8623f5c3c83fe08572abed0311c/Screen_Shot_2018-11-14_at_8.12.40_PM.png)

# AWS - API Gateway!

Create API Gateway as per below screenshot -
![alt API](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/7d7815c91dbadf88438047bd2224f589/Screen_Shot_2018-11-14_at_8.22.53_PM.png)

# Test API

###### upperCase Function
![alt upperCase](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/8414f954b801edf0dd51a2a631cc92fd/Screen_Shot_2018-11-15_at_10.46.28_PM.png)
```sh

```
###### greeter Function
![alt greeter](https://gitlab.com/rahul.a0309/my-serverless-project/wikis/uploads/287fe8a7adff5149a926e515de4d690f/Screen_Shot_2018-11-15_at_10.47.15_PM.png)




