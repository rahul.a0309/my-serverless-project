package com.serverless.application.handler.aws;

import org.springframework.cloud.function.adapter.aws.SpringBootRequestHandler;

import com.serverless.application.domain.ApplicationRequest;
import com.serverless.application.domain.ApplicationResponse;

public class ApplicationFunctionHandler extends SpringBootRequestHandler<ApplicationRequest, ApplicationResponse> {
}
