package com.serverless.application.service;

import org.springframework.stereotype.Service;

@Service
public class GreeterService {

    public String apply(String s) {
        return "Hello " + s + ", and welcome to Spring Cloud Function!!!";
    }
}
