package com.serverless.application.service;

import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class UppercaseService {
    
    public String uppercase(final String input) {
        return input.toUpperCase(Locale.ENGLISH);
    }
}
