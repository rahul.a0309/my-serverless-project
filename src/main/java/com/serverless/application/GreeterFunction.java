package com.serverless.application;

import com.serverless.application.domain.ApplicationRequest;
import com.serverless.application.domain.ApplicationResponse;
import com.serverless.application.service.GreeterService;
import org.springframework.stereotype.Component;

import java.util.function.Function;


@Component("greeter")
public class GreeterFunction implements Function<ApplicationRequest, ApplicationResponse> {

    private final GreeterService greeterService;

    public GreeterFunction(final GreeterService greeterService) {
        this.greeterService = greeterService;
    }

    @Override
    public ApplicationResponse apply(final ApplicationRequest applicationRequest) {
        final ApplicationResponse result = new ApplicationResponse();

        result.setResult(greeterService.apply(applicationRequest.getInput()));

        return result;
    }


}
