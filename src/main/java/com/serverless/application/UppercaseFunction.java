package com.serverless.application;

import com.serverless.application.domain.ApplicationRequest;
import com.serverless.application.domain.ApplicationResponse;
import com.serverless.application.service.UppercaseService;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component("uppercaseFunction")
public class UppercaseFunction implements Function<ApplicationRequest, ApplicationResponse> {

    private final UppercaseService uppercaseService;

    public UppercaseFunction(final UppercaseService uppercaseService) {
        this.uppercaseService = uppercaseService;
    }

    @Override
    public ApplicationResponse apply(final ApplicationRequest applicationRequest) {
        final ApplicationResponse result = new ApplicationResponse();

        result.setResult(uppercaseService.uppercase(applicationRequest.getInput()));

        return result;
    }
}
