package com.serverless.application.domain;

public class ApplicationRequest {

    private String input;

    public String getInput() {
        return input;
    }

    public void setInput(final String input) {
        this.input = input;
    }
}
