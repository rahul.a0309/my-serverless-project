package com.serverless.application.domain;

public class ApplicationResponse {

    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(final String result) {
        this.result = result;
    }
}
