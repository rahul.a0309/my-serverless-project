package com.serverless.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainFunctionApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MainFunctionApplication.class, args);
    }
}
